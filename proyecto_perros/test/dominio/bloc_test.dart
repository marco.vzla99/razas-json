import 'package:flutter_test/flutter_test.dart';
import 'package:proyecto_perros/dominio/bloc.dart';
import 'package:bloc_test/bloc_test.dart';
void main() { 
  blocTest<BlocVerificacion, EstadoVerificacion>(
    'emits [MyState] when MyEvent is added.',
    build: () => BlocVerificacion(),
    act: (bloc) => bloc.add(Creado()),
    expect: () => [isA<SolicitandoRaza>()],
  );
}