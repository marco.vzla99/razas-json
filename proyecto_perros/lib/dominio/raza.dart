import 'package:json_annotation/json_annotation.dart';

class Raza{
  final String message;
  final String status;

  Raza({
    required this.message, required this.status
    });

  factory Raza.fromJson(Map<String, dynamic> json)=> 
  Raza(message: json['message'],
   status: json['status']);

   Map<String, dynamic> toJson() => {
    'message': message,
    'status': status
   };
}