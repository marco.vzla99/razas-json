import 'package:bloc/bloc.dart';

class EventoVerificacion { }
class Creado extends EventoVerificacion{ }
class NombreRecibido extends EventoVerificacion{ }
class NombreConfirmado extends EventoVerificacion{ }

class EstadoVerificacion{ }
class Creandose extends EstadoVerificacion{ }
class SolicitandoRaza extends EstadoVerificacion{ }
class EspertandoConfirmacionRaza extends EstadoVerificacion{ }
class MostrarNombre extends EstadoVerificacion{ }

class BlocVerificacion extends Bloc<EventoVerificacion, EstadoVerificacion> {
  BlocVerificacion() : super(Creandose()) {
    on<Creado>((event, emit) {
      emit(SolicitandoRaza());
    });
  }
}